from django import forms
from meal_plans.models import MealPlan


class MealPlanForm(forms.ModelForm):  # combination of model and form
    class Meta:
        model = MealPlan
        fields = [
            "name",
            "date",
            "recipes"  # because of the many2many --> multiselect dropdown box list
        ]
