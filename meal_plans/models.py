from django.db import models
from recipes.models import USER_MODEL


class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateTimeField(null=True)
    owner = models.ForeignKey(
        USER_MODEL,
        null=True,
        related_name="MealPlans",
        on_delete=models.CASCADE,
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="MealPlans")
    # just because you delete the name of the meal plan does not mean the
    # recipe is deleted.

    def __str__(self):
        return f"{self.name}"
