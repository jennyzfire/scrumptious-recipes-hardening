from django.shortcuts import render
from meal_plans.models import MealPlan
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from meal_plans.forms import MealPlanForm
from django.contrib.auth.mixins import LoginRequiredMixin
from recipes.models import Recipe


class MealPlansList(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "list.html"
    context_object_name = "MealPlan"
    paginate_by: 2


class MealPlanDetail(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "detail.html"
    context_object_name = "detail_plan"


class MealPlanCreate(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "new.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plan_list")


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["Meal_Plan"] = MealPlanForm()
        return context
