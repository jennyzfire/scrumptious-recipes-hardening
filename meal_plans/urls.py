from django.urls import path
from meal_plans.views import MealPlansList
from meal_plans.views import MealPlanCreate
from meal_plans.views import MealPlanDetail

urlpatterns = [
    path("", MealPlansList.as_view(), name="meal_plan_list"),
    path("create/", MealPlanCreate.as_view(), name="create_plan"),
    path("<int:pk>", MealPlanDetail.as_view(), name="detail_plan"),
    # path("meal_plans/<int:pk>/edit", name="edit_plan"),
    # path("meal_plans/<int:pk>/delete", name="delete_plan")
]
