# Generated by Django 4.0.3 on 2022-09-01 03:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meal_plans', '0002_mealplandetail_alter_mealplan_owner_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mealplan',
            name='date',
            field=models.DateTimeField(null=True),
        ),
    ]
